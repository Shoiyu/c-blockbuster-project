#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include <ctype.h>
#define MAX_FILMES 50

// Cuidado com a identação... É necessario utilizar mais tabulações
// Ex de como poderia ficar essa struct identada:
// typedef struct {
//     char titulo[50];
//     char genero[50];
//     char sinopse[100];
//     int status[1];
//     float nota[4];
//     int ativo;
// } Filme;
typedef struct{
    char titulo[50];
    char genero[50];
    char sinopse[100];
    int status[1];
    float nota[4];
    int ativo;
}Filme;
Filme filmes[MAX_FILMES];

void menu();
void cadastro();
void pesquisar();
void editar();
void excluir();
void listar();
FILE* abrirArquivo();
FILE* fecharArquivo();

// Identação
int main()
{  Filme filmes;
    menu(filmes);
    return 0;
}

// Identação
void menu(Filme f){
    int ativo;
    int options;
    do{ 
        system("clear");
        printf("\n==================================================\n");
        printf("                 BLOCK BUSTER v2.4.3.0             \n");
        printf("==================================================\n\n");
 
        printf("1 - Cadastrar\n");
        printf("2 - Pesquisar\n");
        printf("3 - Editar\n");
        printf("4 - Excluir\n");
        printf("5 - Listar\n");
        printf("0 - Sair\n\n");
        scanf("%d", &options);
        getchar(); // Pq?
          //IDENTAÇÃO

        switch(options){
            case 1:
                cadastro(f);
            break;

            case 2:
                pesquisar();
            break;

            case 3:
                editar();
            break;

            case 4:
                excluir();
            break;

            case 5:
                listar();
                system("pause");
            break;

            break;
        }
    }while(options != 0);   
    // Cuidado com os espaçamentos. 
    // Sempre procure utilizar espaços entre valores e operadores. 
   // Ex: a != b
}

void cadastro(Filme f){
    // Será que não há uma forma de ler os valores diretamente 
    // na ultima posição livre de filmes? Isso evitaria a criação de
    // todas as variaveis abaixo, poupando memoria.
    int options;
    FILE* dados;
    dados = abrirArquivo('a',"cadastro.txt");
    fprintf(dados, "%s %s %s %f %n", f.titulo, f.genero, f.sinopse, f.nota[0], f.status[0]);
    do{ 
         getchar();
        system("clear");
        printf("\n==================================================\n");
        printf("                CADASTRO                            \n");
        printf("==================================================\n\n");

        printf("Digite o título do filme: \n");
        fgets(f.titulo, sizeof(MAX_FILMES), stdin);

        printf("Digite o genero do filme: \n");
        fgets(f.genero, sizeof(MAX_FILMES), stdin);

        printf("Digite o sinopse do filme: \n");
        fgets(f.sinopse, sizeof(MAX_FILMES), stdin);

        printf("Digite a nota do filme: \n");
        scanf("%f", &f.nota[0]);
        
        printf("Digite o status(1 - locado 0 - disponível): \n");
        scanf("%d", &f.status[0]);
    
        for(int i=0;i<MAX_FILMES;i++)
        {// espaçamentos
            if(filmes[i].ativo == 0){// espaçamentos
                strcpy(filmes[i].titulo, f.titulo);// espaçamentos
                strcpy(filmes[i].genero, f.genero);// espaçamentos
                strcpy(filmes[i].sinopse, f.sinopse);// espaçamentos
                filmes[i].status[0] = f.status[0];// espaçamentos
                filmes[i].nota[0] = f.nota[0];// espaçamentos
                filmes[i].ativo = 1;// espaçamentos
                break;
            //IDENTAÇÃO
            } 
        }   
        printf("1 - Continuar \n0 - Sair\n");
        scanf("%d", &options);
        getchar();
        FILE* fecharArquivo();
    }while(options != 0);
}
void pesquisar(){
// Nenhuma dessas variaveis (exceto options e titulo) é utilizada... Pq elas estão aqui?
    char titulo[50];
    int options;
    do{     
        system("clear");
        printf("\n====================================================\n");
        printf("                PESQUISA DE CADASTROS                 \n");
        printf("====================================================\n\n");

        printf("Digite o nome do filme: ");
        fgets(titulo,sizeof(titulo), stdin);
        for (int i = 0; i < MAX_FILMES; i++)
        {   
            // Se eu pesquisar "teste", ele não acha o filme "Teste"
            if(strstr(filmes[i].titulo, titulo) != NULL)
            {   
                for(int i = 0; titulo[i]; i++){
                    titulo[i] = tolower(titulo[i]);
            }
                printf("\nID: %d\n", i+1);
                printf("\nTitulo: %s\n", filmes[i].titulo);
                printf("Genero: %s\n", filmes[i].genero);
                printf("Sinopse: %s\n", filmes[i].sinopse);
                printf("Nota: %0.1f\n\n", filmes[i].nota[0]);
                filmes[i].status[0] = 1 ? printf("LOCADO") : printf("DISPONÍVEL"); // Poderia fazer um ternario para mostrar "locado" ou "disponivel" dependendo do valor
                printf("------------------------------\n\n");
            }
        }
        printf("\n1 - para pesquisar novamente\n0 - para sair\n");
        scanf("%d", &options);
        getchar();     
    }while(options != 0);
}

void editar(){
    char titulo[50];
    char genero[50];
    char sinopse[100];
    float nota[4];
    int status[1];
    int options;
    do{
        system("clear");
        printf("\n====================================================\n");
        printf("                EDIÇÃO DE CADASTROS                 \n");
        printf("====================================================\n\n");

        printf("Digite o nome do filme: ");
        fgets(titulo, sizeof(titulo), stdin);
            for (int i = 0; i < MAX_FILMES; i++)
        {
            // essa função de search por titulo tbm é utilizada em outro lugar? 
            // Se sim, acho que poderia transformar em uma função utilitaria
            if(strstr(filmes[i].titulo,titulo) != NULL)
            {
                printf("ID: %d\n", i+1);
                printf("Titulo atual: %s\n", filmes[i].titulo);
                printf("\n\nNovo Título: \n"); 
                fgets(titulo, sizeof(titulo),stdin);
                printf("Genero atual: %s\n", filmes[i].genero);
                printf("\n\nNovo genero: \n"); 
                fgets(genero, sizeof(genero),stdin);
                printf("Sinopse atual: %s\n", filmes[i].sinopse);
                printf("\n\nNova sinopse: \n"); 
                fgets(sinopse, sizeof(sinopse), stdin);
                printf("\nNota atual: %0.1f\n\n", filmes[i].nota[0]);
                printf("\n\nNova nota: \n"); 
                scanf("%f", &nota[0]);
                printf("Status atual: %d\n", filmes[i].status[0]);
                printf("\n\nNovo status: \n"); 
                scanf("%d", status);

                    for(int i = 0; i<MAX_FILMES; i++){

                        if(filmes[i].ativo == 0)
                        {
                            strcpy(filmes[i].titulo,titulo);
                            strcpy(filmes[i].genero,genero);
                            strcpy(filmes[i].sinopse,sinopse);
                            filmes[i].status[0] = status[0];
                            filmes[i].nota[0] = nota[0];
                            filmes[i].ativo = 1;
                        } 
                    }   
                printf("------------------------------\n\n");
            }
        }   getchar();
        printf("\n1 - para editar novamente\n0 - para sair\n");
        scanf("%d", &options);
        getchar();
    }while(options != 0);
}

void excluir(){
    // titulo n está sendo utilizado...
    int options;
    int id;
    listar();
    system("clear");
    printf("\n====================================================\n");
    printf("                EXCLUSÃO DE CADASTROS                 \n");
    printf("====================================================\n\n");
    printf("Digite o ID do filme que deseja exluir: ");
    scanf("%d", &id);
    --id;
    filmes[id].ativo = 0;
    printf("\ncadastro excluído com sucesso! \n 0 - para sair\n");
    getchar();
}

void listar(){
    FILE* dados;
    char titulo[50];
    int options; 
    dados = abrirArquivo('l', "cadastro.txt");
    while(!feof(dados)){
        fscanf(dados, "%s", &titulo[50]);
    }

    do{
        system("clear");
 
        printf("\n====================================================\n");
        printf("                  LISTAGEM DE CADASTROS                 \n");
        printf("====================================================\n\n");
 
        for (int i = 0; i < MAX_FILMES; i++)
        {   
            if(filmes[i].ativo == 1)
            {
                printf("ID: %d\n", i + 1);
                printf("Titulo: %s\n", filmes[i].titulo);
                printf("Genero: %s\n", filmes[i].genero);
                printf("Sinopse: %s\n", filmes[i].sinopse);
                printf("Nota: %0.1f\n\n", filmes[i].nota[0]);
                printf("Status: %n\n", filmes[i].status);
                printf("------------------------------\n\n");
            }
        }
        printf("0 - Sair\n");
        scanf("%d", &options);
        getchar();
        FILE* fecharArquivo();
    }while(options != 0);     
}
FILE* abrirArquivo(char modo, char caminho[30]){
    FILE* dados;
    switch(modo){
        case 'g':
            dados = fopen(caminho, "wt");
            break;
        case 'l':
             dados = fopen(caminho, "rt");
             break;
        case 'a':
             dados = fopen(caminho, "a");
             break;
    }
    if(dados == NULL){
        printf("não foi possível ler o arquivo.");
        exit(0);
    }
    return dados;
}
FILE* fecharArquivo(FILE* dados){
    fclose(dados);
    return dados;
}

